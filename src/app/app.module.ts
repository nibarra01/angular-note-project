import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { InputComponentComponent } from './components/input-component/input-component.component';
import { OutputComponentComponent } from './components/output-component/output-component.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { IndexComponent } from './components/index/index.component';
import { FormsModule } from '@angular/forms';
import { DeletThisComponent } from './components/delet-this/delet-this.component';
import { EditComponent } from './components/edit/edit.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LogComponent } from './components/log/log.component';

@NgModule({
  declarations: [
    AppComponent,
    InputComponentComponent,
    OutputComponentComponent,
    IndexComponent,
    DeletThisComponent,
    EditComponent,
    LogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
