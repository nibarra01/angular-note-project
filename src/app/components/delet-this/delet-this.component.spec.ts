import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeletThisComponent } from './delet-this.component';

describe('DeletThisComponent', () => {
  let component: DeletThisComponent;
  let fixture: ComponentFixture<DeletThisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeletThisComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeletThisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
