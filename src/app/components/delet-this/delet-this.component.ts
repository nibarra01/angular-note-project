import { Component, OnInit } from '@angular/core';
import { NoteService } from 'src/app/services/note-service.service';
import { Note } from 'src/stuff/note';

@Component({
  selector: 'app-delet-this',
  templateUrl: './delet-this.component.html',
  styleUrls: ['./delet-this.component.css']
})
export class DeletThisComponent implements OnInit {

  noteList_deleteComponent: Note[];
  selectedID : number;
  
  constructor(private NOTES:NoteService) { }

  ngOnInit(): void {
    this.subscribe();
  }
  subscribe(){
    this.NOTES.getNotes().subscribe(n => this.noteList_deleteComponent = n);
  }

  select(selected:Note){
    this.selectedID = selected.id
  }

  removeNote(){
    this.NOTES.destroyNote(this.selectedID);
  }

}