import { Component, OnInit } from '@angular/core';
import { NoteService } from 'src/app/services/note-service.service';
import { Note } from 'src/stuff/note';

@Component({
  selector: 'app-output-component',
  templateUrl: './output-component.component.html',
  styleUrls: ['./output-component.component.css']
})
export class OutputComponentComponent implements OnInit {

  noteList_outputComponent: Note[];
  constructor(private NOTES:NoteService) { }

  ngOnInit(): void {
    this.subscribe();
  }
  subscribe(){
    this.NOTES.getNotes().subscribe(n => this.noteList_outputComponent = n);
  }

}
