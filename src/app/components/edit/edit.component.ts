import { Component, OnInit } from '@angular/core';
import { NoteService } from 'src/app/services/note-service.service';
import { Note } from 'src/stuff/note';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  editID : number;
  editIndex : number;
  editTitle : string;
  editContents : string;
  tag1 : boolean = false;
  tag2 : boolean = false;
  tag3 : boolean = false;
  tag4 : boolean = false;
  tag1s : string = "Tag 1";
  tag2s : string = "Tag 2";
  tag3s : string = "Tag 3";
  tag4s : string = "Tag 4";
  tags : string[] = [];

  noteList_editComponent : Note[];
  
  constructor(private NOTES:NoteService) { }
  
  ngOnInit(): void {
    this.subscribe();
  }
  subscribe(){
    this.NOTES.getNotes().subscribe(n => this.noteList_editComponent = n);
  }

  select(selected:Note){
    this.editID = selected.id;
    for (let x = 0; x < this.noteList_editComponent.length; x++){
      if(selected == this.noteList_editComponent[x]){
        this.editIndex = x;
        break;
      }
  
    }
    this.editTitle = this.noteList_editComponent[this.editIndex].title;
    this.editContents = this.noteList_editComponent[this.editIndex].note;
    this.tag1 = this.noteList_editComponent[this.editIndex].category.includes("Tag 1");
    this.tag2 = this.noteList_editComponent[this.editIndex].category.includes("Tag 2");
    this.tag3 = this.noteList_editComponent[this.editIndex].category.includes("Tag 3");
    this.tag4 = this.noteList_editComponent[this.editIndex].category.includes("Tag 4");

  }

  confirm(){

    if (this.tag1){
      this.tags.push(this.tag1s);
    }
    if (this.tag2){
      this.tags.push(this.tag2s);
    }
    if (this.tag3){
      this.tags.push(this.tag3s);
    }
    if (this.tag4){
      this.tags.push(this.tag4s);
    }

    const note:Note = {
      id:this.editID,
      title:this.editTitle,
      note:this.editContents,
      category:this.tags
    }

    this.NOTES.editNote(note);

    this.editID =null;
    this.editIndex =null;
    this.editTitle =null;
    this.editContents =null;
    this.tag1 = false;
    this.tag2 = false;
    this.tag3 = false;
    this.tag4 = false;
    this.tags = [];
  }
}
