import { Component, OnInit } from '@angular/core';
import { title } from 'process';
import { NoteService } from 'src/app/services/note-service.service';
import { NotificationService } from 'src/app/services/notification-service.service';
import { Note } from 'src/stuff/note';

@Component({
  selector: 'app-input-component',
  templateUrl: './input-component.component.html',
  styleUrls: ['./input-component.component.css']
})
export class InputComponentComponent implements OnInit {

  newID:number = 0;
  titleInput : string;
  noteInput : string;
  tags : string[] = [];
  tag1 : boolean;
  tag1s: string = "Tag 1";
  tag2 : boolean;
  tag2s: string = "Tag 2";
  tag3 : boolean;
  tag3s: string = "Tag 3";
  tag4 : boolean;
  tag4s: string = "Tag 4";
  newNote : Note;
  noteList_inputComponent : Note[];
  isEmpty : boolean;
  

  constructor(private NOTES:NoteService) { }

  ngOnInit(): void {
    this.subscribe();
  }
  subscribe(){
    this.NOTES.getNotes().subscribe(n => this.noteList_inputComponent = n);
  }

  submit():void{
    this.findID();
    if (this.tag1){
      this.tags.push(this.tag1s);
    }
    if (this.tag2){
      this.tags.push(this.tag2s);
    }
    if (this.tag3){
      this.tags.push(this.tag3s);
    }
    if (this.tag4){
      this.tags.push(this.tag4s);
    }

    this.newNote = {
      id:this.newID,
      title:this.titleInput,
      note:this.noteInput,
      category:this.tags
    }

    this.NOTES.addNote(this.newNote);

    this.newNote = null;
    this.tags = [];
    this.titleInput = null;
    this.noteInput = null;
    this.tag1 = null;
    this.tag2 = null;
    this.tag3 = null;
    this.tag4 = null;
  }

  findID(){
    console.log("="+this.newID+"=")
    if (this.noteList_inputComponent.length >= 1){
      for (let x = 0; x < this.noteList_inputComponent.length; x++){
        if (this.newID < this.noteList_inputComponent[x].id){
          this.newID = this.noteList_inputComponent[x].id;
        }
        console.log("*"+this.newID + "*")
      }
      this.newID += 1;
    } else {
      this.newID = 1;
    }
  }

}
