import { Component, OnInit } from '@angular/core';
import { NotificationService } from 'src/app/services/notification-service.service';

@Component({
  selector: 'app-log',
  templateUrl: './log.component.html',
  styleUrls: ['./log.component.css']
})
export class LogComponent implements OnInit {

  constructor(public logfile:NotificationService) { }

  ngOnInit(): void {
  }

}
