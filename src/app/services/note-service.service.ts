import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { NoteList } from 'src/stuff/listOfNotes';
import { Note } from 'src/stuff/note';
import { NotificationService } from './notification-service.service';

@Injectable({
  providedIn: 'root'
})
export class NoteService {

  allNotes:Note[] = [];

  constructor(private ns : NotificationService) { }

  getNotes(): Observable<Note[]>{
    this.ns.newNotification("Notes loaded.")
    return of (this.allNotes);
  }

  addNote(new_note : Note){
    this.allNotes.push(new_note);
    this.ns.newNotification("New note added.")
  }

  destroyNote(deleteThis:number){
    for(let x = 0; x < this.allNotes.length; x++){
      if (deleteThis == this.allNotes[x].id){
        this.allNotes.splice(x,1);
        this.ns.newNotification(`Note at index ${x} deleted.`);
        break;
      }
    }
  }

  editNote(edits:Note){
    for(let x = 0; x < this.allNotes.length; x++){
      if(edits.id == this.allNotes[x].id){
        this.allNotes[x] = edits;
        this.ns.newNotification(`Note at index ${x} updated.`);
        break;
      }
    }
  }
}
