import { NgModule, Output } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DeletThisComponent } from './components/delet-this/delet-this.component';
import { EditComponent } from './components/edit/edit.component';
import { IndexComponent } from './components/index/index.component';
import { InputComponentComponent } from './components/input-component/input-component.component';
import { OutputComponentComponent } from './components/output-component/output-component.component';

const routes: Routes = [
  {
    path:'',
    redirectTo: "/index", pathMatch:'full'
  },
  {
    path:"index",
    component:IndexComponent
  },
  {
    path:"Create",
    component:InputComponentComponent
  },
  {
    path:"Read",
    component:OutputComponentComponent
  },
  {
    path:"Destroy",
    component:DeletThisComponent
  },
  {
    path:"Update",
    component:EditComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
